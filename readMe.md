
###  Prerequisites

1. python2 or python3

###  Installation

1. go to the web folder and open a terminal there

1. if you have python2 run `python -m SimpleHTTPServer`

1. if you have python3 run `python3 -m http.server`

1. then run localhost:8000 and you'll be directed to the dictionary service

###  Usage

1. searching for an existing word in the dictionary will show the Bengali meaning

1. a word that doesn't exist will return 'word doesn't exist' 