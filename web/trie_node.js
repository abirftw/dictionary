class TrieNode {
    #is_end;
    #arr;
    char_code;
    constructor() {
        this.#is_end = false;
        this.#arr = new Array(26).fill(null);
    }
    setWord() {
        this.#is_end = true;
    }
    setNode(index, node) {
        try {
            node.char_code = index;
            this.#arr[index] = node;
        } catch (error) {
            console.log(error);
        }
    }
    getNode(index, node) {
        try {
            return this.#arr[index];
        } catch (error) {
            console.log(error);
        }
    }
    isWord() {
        return this.#is_end;
    }
};
function addWordToTrie(word) {
    let trie_node = root_node;
    for (let index = 0; index < word.length; index++) {
        let node_index = word.charCodeAt(index) - 'a'.charCodeAt(0);
        if (trie_node.getNode(node_index) == null) {
            trie_node.setNode(node_index, new TrieNode());
        }
        trie_node = trie_node.getNode(node_index);
    }
    trie_node.setWord();
}