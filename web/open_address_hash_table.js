class OpenAddressTable {
    #m1;
    #m2;
    #dict = []
    constructor(m1, m2) {
        this.#m1 = m1;
        this.#m2 = m2;
    }
    #getNextSlot(key, slot) {
        return (key + slot * (key % this.#m2)) % this.#m1;
    }
    #getKey(word) {
        let hashedVal = 0;
        for (let i = 0; i < word.length; i++) {
            hashedVal = (hashedVal * 128) % this.#m1;
            hashedVal = (hashedVal + word.charCodeAt(i)) % this.#m1;
        }
        return hashedVal;
    }
    addWord(word) { //returns the slot where the word was inserted
        const key = this.#getKey(word);
        let slot = key;
        while (this.#dict[slot]) {
            slot = this.#getNextSlot(key, slot);
        }
        this.#dict[slot] = word;
        return slot;
    }
    searchWord(word) { //returns the slot if found else -1
        const key = this.#getKey(word)
        let slot = key;
        while (this.#dict[slot]) {
            if (this.#dict[slot] == word) {
                return slot;
            }
            slot = this.#getNextSlot(key, slot);
        }
        return -1;
    }
}